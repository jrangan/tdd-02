const parse = (table) => {
  const rowsAndHeaders = table.split("\n");
  console.log(rowsAndHeaders);
  if (rowsAndHeaders === 0) {
    return { header: [], rows: [] };
  } else if (rowsAndHeaders.length === 1) {
    return { header: [getTokens(rowsAndHeaders[0])], rows: [] };
  } else {
    const header = getTokens(rowsAndHeaders[0]);
    const row = getTokens(rowsAndHeaders[1]);
    return {
      header: [header],
      rows: [{ [header]: row }],
    };
  }
};

const getTokens = (token) => {
  const headersOrRowsTokens = token.split(" ");
  return headersOrRowsTokens[1];
};

module.exports = parse;
